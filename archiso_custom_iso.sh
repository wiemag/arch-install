#!/bin/bash

VERSION='0.98'
RUN=0
CLEAN=0 	# Clean temporary profile and workdir folders (1)
			# switched on if RUN is set on
root_pw='archiso!'
PF=''	# archiso profile folder
		# the script creates a temporary directory later on
WD=''	# archiso work directory where actual work is done
		# the script creates a temporary directory later on
FONT=''	# console-font name sourced from /etc/vconsole.conf if it exists
FONTPKG='' 	# istallation package name for the $FONT
PL='packages.x86_64' # list of packages to be installed in archiso 
grub_timeout=3
debug=0
i=0			# disposable integer variable


usg_msg(){
echo -e "\nRun:\n\t${0##*/} [-d][-r] | [-h|--help]\nor"
echo -e "\t${0##*/} -c profile_folder\n"
cat <<INFO
-c = use an existing profile_folder and create the archiso.iso
     It implies -r and, if successful, removing temporary files.

-d = debug mode (More info printed)
     You can modify the archiso package list only in the debug mode.

Without -r
  The script creates a temporary ISO profile_folder with
  parameters that modify the stock Arch Linux ISO.

With -r
  The script creates a customised Arch installation iso image.

• sshd.service is switched on
• SSH authorization key is added from ${HOME}/.ssh/id-ed2559.pub"
• Password for the root is set up:  $root_pw
• Locale files are copied to image/etc/
• Some aliases are added to image/root/.zshrc

The output directory:  /tmp
INFO
}

function the_action(){
if ((RUN))
then
	echo -e "\nAbout to create the live arch-iso."
	echo "  # mkarchiso -v -w tmp_work_dir -o output_dir tmp_profile_dir"
	echo -e "  -o /tmp"
	echo -e "  tmp_profile_dir = $PF (temporary)\n"
	if 	read -en1 -p "Press any key to continue. "
	then
		WD=$(mktemp -d) # create the work_dir
		LANG=C sudo mkarchiso -v -w "$WD" -o /tmp "$PF"
		(($?)) && {
			CLEAN=0;
			echo -e "\nError.\nCleaning temporary folders is switched off.";
		} || echo Success! The ISO image has been created.
	fi
else
	echo -e "\nUse the '-r' scritpt parameter to create the ISO."
fi

((CLEAN)) && {
	echo Cleaning...
	[[ -d "$WD" ]] && LANG=C sudo rm -r "$WD";
	LANG=C sudo rm -r "$PF";
} || {
	echo "Archiso profile folder:  $PF"
	[[ -d "$WD" ]] && echo "Working folder:  $WD"
}
}

### START-UP SECTION

[[ "${1:-}" == '--help' ]] && { usg_msg; exit 0;}

while getopts "drhvc:" flag
do
	case "$flag" in
		d) debug=1;;
		r) RUN=1; CLEAN=1;;
		h) usg_msg; exit 0;;
		v) echo -e "\n${0##*/}, version ${VERSION}" && exit;;
		c) RUN=1; CLEAN=1; PF="$OPTARG"; the_action; exit $?;;
	esac
done
# Remove the options parsed above.
shift `expr $OPTIND - 1`

# Install the archiso package
hash mkarchiso 2>/dev/null 1>&2 || {
	echo Package \'archiso\' not installed.;
	exit 9;
}


### PREPARE CUSTOM PROFILE

# The profile structure is documented in /usr/share/doc/archiso/README.profile.rst
PF="$(mktemp -d)" # Profile folder
2>/dev/null pushd "$PF" 1>&2
((debug)) && >&2 echo -e "\nDEBUG:  archiso profile folder = $PF"

[[ -d /usr/share/archiso/configs/releng ]] &&
	cp -r /usr/share/archiso/configs/releng/* . || {
		>&2 echo Can\'t copy basic profile files.;
		exit 3;
	}
# Edit packages.x86_64 to select which packages to install
# on the live system, listing packages line by line.
((debug)) && {
	read -n1 -p " View and edit package list for the live-iso" 
	vim ./"${PL}"
}

# Uncomment repositories that you need in ./pacman.conf
#vim ./pacman.conf

## Add files to image:  directory airootfs will be the root of live img


((debug)) && echo "$((++i)).  DIRECTORIES"
mkdir -p ./airootfs/etc/{iwd,pacman.d,ssh}
mkdir -p ./airootfs/etc/systemd/system/multi-user.target.wants
mkdir -p ./airootfs/root/.ssh
mkdir -p ./airootfs/var/lib/iwd


((debug)) && echo "$((++i)).  LOCALE"
f='/usr/share/kbd/consolefonts'
FONTPKG=''	# installation pkg name for the font in /etc/vconsole.conf
[[ -r /etc/vconsole.conf ]] && {
	source /etc/vconsole.conf 	# among others sources the FONT= variable
	if test -r "$f"/${FONT}.*|head -1
	then
		FONTPKG="$(pacman -Qqo $(command ls -1 "$f"/${FONT}.*|head -1))"
# FONTPKG could be '' here.
# It is unlikely. For the time being, I'm ignoring it.
		# if not in archiso package list, add it to the package list
		grep -q "$FONTPKG" "./${PL}" || echo "$FONTPKG" >> "./${PL}"
	fi
}
for f in locale.conf locale.gen vconsole.conf;
do
	[[ -r "/etc/$f" ]] && cp "/etc/$f" ./airootfs/etc/ ||
		>&2 echo WARNING: /etc/$f not copied to the iso profile.
done;


echo "$((++i)).  IWD:  WiFi NETWORK WITH iwd.service"
# The iwd.service is already copied from ...archiso/configs/releng/
#ln -s /usr/lib/systemd/system/iwd.service ./airootfs/etc/systemd/system/multi-user.target.wants/
#ln -s /usr/lib/systemd/system/systemd-resolved.service ./airootfs/etc/systemd/system/multi-user.target.wants/
PSKs=($(LANG=C sudo find /var/lib/iwd -type f -name '*psk'))
echo
for f in "${PSKs[@]}"
do
	((debug)) && >&2 echo "$f"' -> ./airootfs/var/lib/iwd/'"${f##*/}"
	LANG=C sudo sed '/^Passphrase=/d' "$f" > "./airootfs/var/lib/iwd/${f##*/}"
done
iwdConf="[General]\nEnableNetworkConfiguration=true\n\n"
iwdConf+="[Network]\nNameResolvingService=systemd\nEnableIPv6=false\n\n"
#iwdConf+="[Settings]\nAutoConnect=true\n\n" 	# Not necessary
iwdConf+="[Scan]\nDisablePeriodicScan=false"	# Necessary
echo -e "$iwdConf" > ./airootfs/etc/iwd/main.conf


((debug)) && echo "$((++i)).  ARCH LINUX MIRROR LIST"
cp /etc/pacman.d/mirrorlist ./airootfs/etc/pacman.d/


echo "$((++i)).  SSHD and SSH AUTHORISED KEYS"
# SSHD -- the ssh.service is already copied from ...archiso/configs/releng/
#ln -s /usr/lib/systemd/system/sshd.service ./airootfs/etc/systemd/system/multi-user.target.wants/
for f in ${HOME}/.ssh/{id_ed25519.pub,root@installmedia.pub}
do 
	[[ -r "$f" ]] &&
		cat "$f" >> ./airootfs/root/.ssh/authorized_keys &&
		((debug)) && >&2 echo "$f "'-> ./airootfs/root/.ssh/authorized_keys'
done


echo "$((++i)).  ROOT PASSWORD:  $root_pw"
hash="$(openssl passwd -6 "$root_pw" | sed 's/\//\\\//g')"
sed -i 's/::/:'"$hash"':/' ./airootfs/etc/shadow


echo "$((++i)).  ADDITIONAL ALIASES"
f="alias lsblkk='lsblk -o name,fstype,size,label,partlabel,mountpoint'"
f+="\nalias efibootmgr='efibootmgr -u'"
f+="\nalias l='ls -alF'"
f+="\nalias s='systemctl'"
f+="\nalias j='sudo journalctl'"
f+="\nalias nocomment='grep -Ev \"^\s*(#|$)\"'"
echo -e "$f" >> ./airootfs/root/.zshrc


echo "$((++i)).  SHORTEN/SET GRUB MENU TIMEOUT TO $grub_timeout seconds"
sed -i '/^timeout=[0-9]*$/s/=.*/=3/' ./grub/grub.cfg

popd 2>/dev/null 1>&2


# FINAL COMMANDS
the_action
